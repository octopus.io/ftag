# ftag

Python project for file tagging system.

The goal of this project is to have a system
to store file using tags instead of directory.

If you are looking for something more mature 
I recommend to you to look at TMSU.

### dependency:

I think i will use these dependency for my project:

- sqlite
- orm (peewee ?)
- fuse (fusepy)
